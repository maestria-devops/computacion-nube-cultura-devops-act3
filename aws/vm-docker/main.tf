provider "aws" {
  region = var.region
}

data "aws_availability_zones" "available" {}

data "aws_ami" "linux_ubuntu" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name = "name"
    #values = ["amzn-ami-hvm-*-x86_64-gp2"]
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

locals {
  hostname = format("%s-%s", var.environment, var.machine_name)
  vpc_name = format("%s-vpc", local.hostname)

  vpc_cidr = "10.0.0.0/16"
  azs      = slice(data.aws_availability_zones.available.names, 0, 3)

  tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}

module "key_pair" {
  source             = "terraform-aws-modules/key-pair/aws"
  key_name           = format("%s-key", local.hostname)
  create_private_key = true
}

resource "null_resource" "pem_file" {
  provisioner "local-exec" {
    command = "echo '${module.key_pair.private_key_pem}' > ./${module.key_pair.key_pair_name}.pem"
  }

  provisioner "local-exec" {
    command = "chmod 600 ./${module.key_pair.key_pair_name}.pem"
  }

}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 5.0"

  name = local.vpc_name
  cidr = local.vpc_cidr

  azs             = local.azs
  private_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 4, k)]
  public_subnets  = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 48)]

  # Single NAT Gateway
  enable_nat_gateway = true
  single_nat_gateway     = true
  #one_nat_gateway_per_az = false

  tags = local.tags
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = format("%s-sg", local.hostname)
  description = format("Security group for %s EC2 instance.", local.hostname)
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-tcp", "http-80-tcp"]
  egress_rules        = ["all-all"]

  tags = local.tags
}

data "template_file" "startup_script" {
  template = file("${path.module}/startup-script.sh")

  vars = {
    ADDITIONAL_USER_DATA = var.user_data
  }
}

module "ec2_instance" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name                        = local.hostname
  key_name                    = module.key_pair.key_pair_name
  ami                         = data.aws_ami.linux_ubuntu.id
  instance_type               = var.machine_type
  monitoring                  = true
  availability_zone           = element(module.vpc.azs, 0)
  subnet_id                   = element(module.vpc.public_subnets, 0)
  vpc_security_group_ids      = [module.security_group.security_group_id]
  associate_public_ip_address = true
  user_data_base64            = base64encode(data.template_file.startup_script.rendered)
  user_data_replace_on_change = true

  tags       = local.tags
  depends_on = [module.vpc]


}
