locals {
  user_data = <<-EOT
    sudo docker pull wordpress
    sudo docker run -d --restart always --name wordpress-container -p 80:80 wordpress
  EOT
}

module "vm_wordpress" {
  source = "../../vm-docker"


  region       = "us-east-1"
  environment  = "dev"
  main_zone    = "us-central1-a"
  machine_name = "wordpress"
  machine_type = "t2.small"
  user_data    = local.user_data
}
